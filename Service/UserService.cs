﻿using DB.Interface;
using Model;
using System;
using System.Threading.Tasks;

namespace Service
{
    public class UserService
    {
        private readonly IDbActions<UserTodo> _userRepo;
        public UserService(IDbActions<UserTodo> userRepo) {
            _userRepo = userRepo;
        }

        public UserService()
        {
        }

        public Task insert(UserTodo toInsert)
        {
            return _userRepo.insert(toInsert);
        }
        public UserTodo login(UserTodo user)
        {
            return _userRepo.Login(user.UserName,user.password);
        }
        public Task<UserTodo[]> getAll() {
            return _userRepo.getAll();
        }

        public UserTodo get(UserTodo userTodo)
        {
            return _userRepo.get(userTodo.IdUsuario).Result;
        }
    }
}
