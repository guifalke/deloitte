﻿using DB.Interface;
using Model;
using System.Threading.Tasks;

namespace Service
{
    public class TodoService
    {
        private readonly IDbActions<Todo> _TodoRepo;
        private readonly IDbActions<UserTodo> _UserRepo;
        public TodoService(IDbActions<Todo> Repo,IDbActions<UserTodo> UserRepo)
        {
            _TodoRepo = Repo;
            _UserRepo = UserRepo;
        }
        public async Task<Todo[]> getAll(int id)
        {
            Todo[] todo =await _TodoRepo.getByUser(id);
            if (todo == null)
                return new Todo[0];
            return todo;
        }
        public Task insert(Todo todo)
        {
            return _TodoRepo.insert(todo);
        }
        public Task update(Todo todo)
        {
            return _TodoRepo.update(todo);
        }
        public Task delete(Todo todo)
        {
            return _TodoRepo.delete(todo);
        }
    }
}
