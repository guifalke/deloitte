﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Todo
    {
        [Key]
        public int IdTodo { get; set; }
        public string Task { get; set;}
        public string Description { get; set; }
        public DateTime LastUpdate { get; set; }
        public int IdUsuario { get; set; }

    }
}