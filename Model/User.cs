﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class UserTodo
    {
        [Key]
        public int IdUsuario { get; set; }
        public string password { get; set; }
        public string UserName { get; set; }
    }
}
