﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebUi
{
    public class Request
    {
        public async Task<T> post<T>(string url, T item)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");

                string response =await httpClient.GetStringAsync(new Uri(url));

                return JsonConvert.DeserializeObject<T>(response);
            }
        }
        public async Task<T> post<T>(string url,string authorization ,T item)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", authorization);

                string response =await httpClient.GetStringAsync(new Uri(url));

                return JsonConvert.DeserializeObject<T>(response);
            }
        }
    }
}
