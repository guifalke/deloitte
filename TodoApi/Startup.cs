﻿using DB;
using DB.Interface;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Model;
using Service;

namespace TodoApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DBConfig>(g => g.UseSqlServer(Configuration.GetConnectionString("ConnectString")));

            services.AddTransient<IDbActions<UserTodo>, UserRepo>();
            services.AddTransient<IDbActions<Todo>, TodoRepo>();

            services.AddTransient<UserService>(); 
            services.AddTransient<TodoService>();

            services.AddAuthentication("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);
            services.AddCors(options =>
            {
                options.AddPolicy("all",
                builder =>
                {
                    builder.AllowAnyHeader();
                    builder.WithOrigins(Configuration["AllowedHosts"]);
                });                
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {               
                app.UseHsts();
            }
            
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<DBConfig>();
                
                context.Database.EnsureCreated();

                context.Database.GetAppliedMigrations();
            }
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
