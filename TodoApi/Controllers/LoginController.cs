﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Model;
using Service;
namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("all")]
    [AllowAnonymous]
    public class LoginController : ControllerBase
    {
        private readonly UserService _userService;
        public LoginController(UserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "1.0.0.0";
        }
        [HttpPost]
        public UserTodo Post([FromBody] UserTodo value)
        {
            UserTodo user =_userService.login(value);
            user.password = "";
            return user;
        }
    }
}
