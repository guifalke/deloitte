﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Service;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("all")]
    [Authorize]
    public class TodoController : ControllerBase
    {
        private readonly TodoService _todoService;
        public TodoController(TodoService todoService)
        {
            _todoService = todoService;
        }
        [HttpGet("{id}")]
        public async Task<Todo[]> Get(int id)
        {
            return await _todoService.getAll(id);
        }
        [HttpPost]
        public async Task Post(Todo todo)
        {
           await _todoService.update(todo);
        }
        [HttpPut]
        public async Task Put(Todo todo)
        {
            await _todoService.insert(todo);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _todoService.delete(new Todo() {IdTodo=id });
        }
    }
}