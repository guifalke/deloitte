﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DB.Interface;
using Microsoft.EntityFrameworkCore;
using Model;

namespace DB
{
    public class TodoRepo : DBConfig, IDbActions<Todo>
    {
        public TodoRepo(DbContextOptions options) : base(options)
        {
        }

        public async Task delete(Todo toDelete)
        {
            Todo todo = await get(toDelete.IdTodo);
            this.Todo.Remove(todo);
            await this.SaveChangesAsync();
        }

        public Task<Todo> get(int ID)
        {
            return this.Todo.FindAsync(ID);
        }

        public Task<Todo[]> getAll()
        {
            return this.Todo.ToArrayAsync();
        }

        public Task<Todo[]> getByUser(int id)
        {
            return this.Todo.Where(g=>g.IdUsuario==id).ToArrayAsync();
        }

        public Task insert(Todo toInsert)
        {
            this.Todo.AddAsync(toInsert).Wait();
            return this.SaveChangesAsync();
        }

        public Todo Login(string userName, string password)
        {
            throw new System.NotImplementedException();
        }

        public Task update(Todo toUpdate)
        {
            Todo todo = get(toUpdate.IdTodo).Result;
            todo.Description = toUpdate.Description;
            todo.Task = toUpdate.Task;
            todo.LastUpdate = toUpdate.LastUpdate;
            this.Todo.Update(todo);
            return this.SaveChangesAsync();
        }
    }
}
