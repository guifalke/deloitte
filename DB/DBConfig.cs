﻿using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Threading.Tasks;

namespace DB
{
    public class DBConfig : DbContext
    {
        public DbSet<UserTodo> UserTodo { get; set; }
        public DbSet<Todo> Todo { get; set; }
        public DBConfig(DbContextOptions options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserTodo>().HasData(
                new UserTodo() {IdUsuario=1,  UserName = "test", password = "pwd123" }
            );
        }
    }
}
