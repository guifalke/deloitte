﻿using DB.Interface;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class UserRepo : DBConfig, IDbActions<UserTodo>
    {
        public UserRepo(DbContextOptions options) : base(options)
        {
        }

        public async Task delete(UserTodo toDelete)
        {
            UserTodo user = await get(toDelete.IdUsuario);
            this.UserTodo.Remove(user);
        }

        public Task<UserTodo> get(int ID)
        {
            return this.UserTodo.FindAsync(ID);
        }
        public UserTodo Login(string userName, string password)
        {
            return this.UserTodo.FirstOrDefault(g=>g.UserName== userName && g.password== password);
        }

        public Task<UserTodo[]> getAll()
        {
            return this.UserTodo.ToArrayAsync();
        }

        public Task insert(UserTodo toInsert)
        {
            this.UserTodo.AddAsync(toInsert);
            return this.SaveChangesAsync();
        }

        public async Task update(UserTodo toUpdate)
        {
            UserTodo user = await get(toUpdate.IdUsuario);
            user.UserName = toUpdate.UserName;
            user.password = toUpdate.password;
            this.UserTodo.Update(user);
        }

        public Task<UserTodo[]> getByUser(int id)
        {
            throw new NotImplementedException();
        }
    }
}
