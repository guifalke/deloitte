﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading.Tasks;

namespace WebApp.Controllers
{
    public class LoginController : Controller
    {
        public Request _request;
        public string apiBase;
        public LoginController(Request request, IConfiguration configuration)
        {
            _request = request;
            apiBase = configuration["apiUrl"];

        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Login(UserTodo user)
        {
            try
            {
                UserTodo logged = await _request.post<UserTodo>(apiBase + "/api/Login", user);
                if (logged is null)
                {
                    TempData["Erro"] = "User not found";
                    return View("Index");
                }
                else
                {
                    HttpContext.Session.SetString("User", JsonConvert.SerializeObject(logged));
                    return RedirectToAction("Index", "ToDo");
                }
            }
            catch
            {
                TempData["Erro"] = "User not found";
                return View("Index");
            }
            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return Json(new{ RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
