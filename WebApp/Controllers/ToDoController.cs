﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model;
using Newtonsoft.Json;

namespace WebApp.Controllers
{
    public class ToDoController : Controller
    {
        public Request _request;
        public string apiBase;
        public ToDoController(Request request, IConfiguration configuration)
        {
            _request = request;
            apiBase = configuration["apiUrl"];
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                string tempU = HttpContext.Session.GetString("User");
                UserTodo user = JsonConvert.DeserializeObject<UserTodo>(tempU);
                string raw = user.UserName + ":" + user.IdUsuario;
                Todo[] toDo = await _request.get<Todo[]>(apiBase + "/api/Todo/" + user.IdUsuario, raw);
                return View(toDo);
            }
            catch(Exception e)
            {
                TempData["Erro"] = e.Message;
                return View("Index");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Insert(Todo todo)
        {
            try
            {
                UserTodo user = JsonConvert.DeserializeObject<UserTodo>(HttpContext.Session.GetString("User"));
                string raw = user.UserName + ":" + user.IdUsuario;
                todo.IdUsuario = user.IdUsuario;
                todo.LastUpdate = DateTime.Now;
                await _request.put<Todo>(apiBase + "/api/Todo", raw, todo);
                return RedirectToAction("Index", "ToDo");
            }
            catch (Exception e)
            {
                TempData["Erro"] = e.Message;
                return View("Index");
            }
        }
        [HttpPost]
        public async Task<IActionResult> Update(Todo todo)
        {
            try
            {
                UserTodo user = JsonConvert.DeserializeObject<UserTodo>(HttpContext.Session.GetString("User"));
                string raw = user.UserName + ":" + user.IdUsuario;
                todo.IdUsuario = user.IdUsuario;
                todo.LastUpdate = DateTime.Now;
                await _request.post<Todo>(apiBase + "/api/Todo", raw, todo);
                return RedirectToAction("Index", "ToDo");
            }
            catch (Exception e)
            {
                TempData["Erro"] = e.Message;
                return View("Index");
            }
        }
        public async Task<IActionResult> Delete(Todo todo)
        {
            try
            {
                UserTodo user = JsonConvert.DeserializeObject<UserTodo>(HttpContext.Session.GetString("User"));
                string raw = user.UserName + ":" + user.IdUsuario;
                await _request.delete<Todo>(apiBase + "/api/Todo/" + todo.IdTodo, raw);
                return RedirectToAction("Index", "ToDo");
            }
            catch (Exception e)
            {
                TempData["Erro"] = e.Message;
                return View("Index");
            }
        }
    }
}