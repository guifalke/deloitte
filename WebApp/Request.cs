﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebApp
{
    public class Request
    {
        public async Task<T> post<T>(string url, T item)
        {
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {

                    httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");

                    HttpResponseMessage response = await httpClient.PostAsync(
                        url,
                        new StringContent(JsonConvert.SerializeObject(item),
                            Encoding.UTF8,
                            "application/json")
                        );
                    if ((int)response.StatusCode != 200)
                        throw new Exception("Erro no webservice");
                    string json = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }catch(Exception e)
            {
                throw;
            }
        }
        public async Task<T> post<T>(string url, string authorization, T item)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                byte[] binary = Encoding.UTF8.GetBytes(authorization);
                string base64 = Convert.ToBase64String(binary);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64);


                HttpResponseMessage response = await httpClient.PostAsync(
                    url,
                    new StringContent(JsonConvert.SerializeObject(item),
                        Encoding.UTF8,
                        "application/json")
                    );
                    if((int)response.StatusCode != 200)
                        throw new Exception("Erro no webservice");
                string json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }

        public async Task put<T>(string url, string authorization, T item)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                byte[] binary = Encoding.UTF8.GetBytes(authorization);
                string base64 = Convert.ToBase64String(binary);
                httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64);


                HttpResponseMessage response = await httpClient.PutAsync(
                    url,
                    new StringContent(JsonConvert.SerializeObject(item),
                        Encoding.UTF8,
                        "application/json")
                    );
                string json = await response.Content.ReadAsStringAsync();
                if ((int)response.StatusCode != 200)
                    throw new Exception("Erro no webservice");
            }
        }
        public async Task<T> get<T>(string url, string authorization)
        {
            byte[] binary = Encoding.UTF8.GetBytes(authorization);
            string base64 = Convert.ToBase64String(binary);
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64);


                HttpResponseMessage response = await httpClient.GetAsync(url);
                string json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(json);
                if ((int)response.StatusCode != 200)
                    throw new Exception("Erro no webservice");
            }
        }
        public async Task delete<T>(string url, string authorization)
        {
            byte[] binary = Encoding.UTF8.GetBytes(authorization);
            string base64 = Convert.ToBase64String(binary);
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64);


                HttpResponseMessage response = await httpClient.DeleteAsync(url);
                string json = await response.Content.ReadAsStringAsync();
                if ((int)response.StatusCode != 200)
                    throw new Exception("Erro no webservice");
            }
        }
    }
}
